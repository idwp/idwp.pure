<?php

// базовая тема на основе bootstrap + jquery
add_action('wp_enqueue_scripts', function (){
	$vendor_dir = get_template_directory_uri().'/vendor';
    $vendor_dir = str_replace(get_site_url(), '', $vendor_dir);
	$theme_dir = get_template_directory_uri().'/assets';
    $theme_dir = str_replace(get_site_url(), '', $theme_dir);

    // jq + bs
    wp_enqueue_script('jquery', $vendor_dir.'/jquery.min.js', [], null, true);
    wp_enqueue_script('popper', $vendor_dir.'/popper.min.js', [], null, true);
	wp_enqueue_script('bootstrap', $vendor_dir.'/bs4/js/bootstrap.min.js', [], null, true);
    wp_enqueue_style('bootstrap', $vendor_dir.'/bs4/css/bootstrap.min.css', [], null);

    // стили сайта
    wp_enqueue_style('theme', $theme_dir.'/css/style.css', [], null);

    // скрипты сайта
	wp_enqueue_script('idwp-js', $theme_dir.'/js/site.js', ['jquery'], null, true);

}, 60);