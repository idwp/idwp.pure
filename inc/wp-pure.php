<?php

// очищаем лишние скрипты и стили
add_action('wp_enqueue_scripts', function () {
	// disable old jquery
	wp_deregister_script('jquery');

	// remove shitmeter
	wp_dequeue_script('wc-password-strength-meter');
	wp_deregister_script('wc-password-strength-meter');

	add_action('wp_head', function () {
		echo '<meta name="developer" content="http://infodesign.by"/>';
	});

	// remove emoji
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');
	// remove gutenshit
	wp_dequeue_style('wp-block-library');
	// remove all other mess
	wp_deregister_script('wp-embed');
	remove_action('wp_head', 'wp_resource_hints', 2);
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_shortlink_wp_head');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
	remove_action('template_redirect', 'rest_output_link_header', 11);
}, 50);

// отключаем комментарии
{
	add_action('wp_before_admin_bar_render', function () {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
	});

	add_action('init', function () {
		remove_post_type_support('post', 'comments');
		remove_post_type_support('page', 'comments');
	}, 100);

	add_action('admin_menu', function () {
		remove_menu_page('edit-comments.php');
		remove_menu_page('edit.php');
	});
}