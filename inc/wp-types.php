<?php

// менюшка
include_once __DIR__.'/bs4navwalker.php';
register_nav_menu('left', 'Основное меню');

// регистрируем сайдбары
add_action('widgets_init', function () {

    register_sidebar([
        'id' => 'sidebar-headline',
        'name' => 'Верхний блок',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    ]);

    register_sidebar([
        'id' => 'sidebar-left',
        'name' => 'Боковой сайдбар',
        'before_widget' => '<section id="%1$s" class="mb-5 widget %2$s">',
        'after_widget' => '</section>',
    ]);

    register_sidebar([
        'id' => 'sidebar-left-banner',
        'name' => 'Баннер под меню',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    ]);

    register_sidebar([
        'id' => 'sidebar-footer',
        'name' => 'Подвал',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    ]);

    add_filter('dynamic_sidebar_params', function ($params) {
        $sidebar_id = $params[0]['id'];

        // добавляем col-X, счетчики и признаки для виджетов в нижнем сайдбаре
        if ($sidebar_id == 'sidebar-footer') {
            static $sidebar_counter = 0;
            $sidebar_counter++;
            $total_widgets = wp_get_sidebars_widgets();
            $sidebar_widgets = count($total_widgets[$sidebar_id]);
            $classes = [
                'col-' . floor(12 / $sidebar_widgets),
                'widget-' . $sidebar_counter . '-' . $sidebar_widgets
            ];
            if ($sidebar_counter === 1) {
                $classes[] = 'widget-first';
            }
            if ($sidebar_counter === $sidebar_widgets) {
                $classes[] = 'widget-last';
            }
            $classes[] = ' ';
            $params[0]['before_widget'] = str_replace(
                'class="', 'class="' . implode(' ', $classes), $params[0]['before_widget']);
        }

        return $params;
    });
});