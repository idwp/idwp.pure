<?php get_header() ?>
    <section class="mainline">
        <div class="container">
            <?php if (have_posts()): ?>
                <main class="mainbox">
                    <?php
                    if (!is_page(43)) {
                        ?>
                        <h1><?php the_title(); ?></h1>
                        <?php
                    }
                    ?>
                    <?php the_post(); ?>
                    <?php the_content(); ?>
                </main>
            <?php endif ?>
        </div>
    </section>
<?php get_footer() ?>